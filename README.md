# Resume template in LaTeX

Credit goes to:
- © 2002 Matthew Boedicker <mboedick@mboedick.org> (original author) http://mboedick.org
- © 2003-2007 David J. Grant <davidgrant-at-gmail.com> http://www.davidgrant.ca
- © 2007-2010 Todd C. Miller <Todd.Miller@courtesan.com> http://www.courtesan.com/todd

Licensed under the 'Creative Commons Attribution-ShareAlike 3.0 Unported License'
(http://creativecommons.org/licenses/by-sa/3.0/).

I've made enough modifications that the old instructions don't apply,
and I'm looking for a job, so of course I don't have the time to write
new ones at the moment.

But you can also view my [Resume](EvanCofsky2017.pdf) and
it's [source](EvanCofsky2017.tex) for usage. And of course
the [actual style](resume.sty) is right here.
